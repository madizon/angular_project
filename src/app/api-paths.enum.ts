export enum ApiPaths {
    Company = '/companies',
    Employee = '/employees',
    Dashboard = '/dashboard',
    Login = '/login'
}
