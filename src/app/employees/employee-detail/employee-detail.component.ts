import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from './../employee.service';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  id = this.route.snapshot.params['id'];
  employee: any = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: EmployeeService
  ) {}


  ngOnInit(): void {
    this.loadEmployee();
  }

  loadEmployee(){
   return this.service.getEmployee(this.id).subscribe((data) => {
       this.employee = data;
    });
  }
}




