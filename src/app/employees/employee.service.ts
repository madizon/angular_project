import { ApiPaths } from './../api-paths.enum';
import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from './employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

     // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

    employeeUrl = `${environment.baseURL}${ApiPaths.Employee}`;

    constructor(private httpClient: HttpClient) { }

    getEmployees(): Observable<Employee>{
      return this.httpClient.get<Employee>(this.employeeUrl);
    }

    getEmployee(id): Observable<Employee> {
      return this.httpClient.get<Employee>(this.employeeUrl + '/' + id);
    }

    createEmployee(employee): Observable<Employee> {
      return this.httpClient.post<Employee>(this.employeeUrl, JSON.stringify(employee), this.httpOptions);
    }

    updateEmployee(employee): Observable<Employee> {
      return this.httpClient.put<Employee>(this.employeeUrl + '/' + employee.id, JSON.stringify(employee), this.httpOptions);
    }

    deleteEmployee(id){
      return this.httpClient.delete<Employee>(this.employeeUrl + '/' + id, this.httpOptions);
    }

}

