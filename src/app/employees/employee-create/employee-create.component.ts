import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from './../employee.service';
import { CompanyService } from './../../companies/company.service';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {

  constructor(private router: Router,
              private service: EmployeeService,
              private companyService: CompanyService) { }

  @Input() employeeDetails = {last_name : null,
                              company_id: null,
                              first_name: null,
                              email: null,
                              phone: null};
  companies: any = {};

  ngOnInit(): void {
    this.loadCompanies();
  }

  loadCompanies(){
    return this.companyService.getCompanies().subscribe((data: {}) => {
      this.companies = data;
    });
 }

  addEmployee() {
    this.service.createEmployee(this.employeeDetails).subscribe((data: {}) => {
      this.router.navigate(['/employees']);
    });
  }
}
