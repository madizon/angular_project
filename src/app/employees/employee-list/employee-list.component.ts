import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './../employee.service';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
    employees: any = [];

    constructor(private service: EmployeeService) { }

    ngOnInit(): void {
      this.loadEmployees();
     }
     
     loadEmployees(){
        return this.service.getEmployees().subscribe((data: {}) => {
          this.employees = data;
        });
     }

     deleteEmployee(id) {
        if (window.confirm('Are you sure you want to delete?')){
           this.service.deleteEmployee(id).subscribe(data => {
            this.loadEmployees();
          });
        }
      }
}



