import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';


const employeesRoutes: Routes = [
  { path: 'login',  component: LoginComponent, data: {skipGuard: true}}
];

@NgModule({
  imports: [
    RouterModule.forChild(employeesRoutes)
  ],
  exports: [
    RouterModule,
  ]
})
export class LoginRoutingModule { }
