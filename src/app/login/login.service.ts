import { ApiPaths } from './../api-paths.enum';
import { environment } from './../../environments/environment';
import { Login } from './login';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loginUrl = `${environment.baseURL}${ApiPaths.Login}`;

  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

  constructor(private httpClient: HttpClient) { }

  login(login): Observable<Login>
  {
    return this.httpClient.post<Login>(this.loginUrl, JSON.stringify(login), this.httpOptions);
  }
}
