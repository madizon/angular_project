import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { TokenService } from './../token.service';
import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private service: LoginService,
              public authState: AuthService,
              private token: TokenService,
              public router: Router) { }

  @Input() loginDetails = {email : null, password: null};
  user: any = [];
  errors = null;

  ngOnInit(): void {
    this.router.navigate(['/dashboard']);
  }

  login() {
    return this.service.login(this.loginDetails).subscribe(
      result => 
      {
        this.responseHandler(result);
      },
      error => 
      { 
        this.errors = error.error;
        this.loginDetails.password = null;
      },
      () => 
      {
        this.authState.setAuthState(true);
        this.router.navigate(['/dashboard']);
      }
    );
  }

   responseHandler(data){
    this.token.handleData(data.access_token);
  }
}
