import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  Dashboards: any = [];
  
  constructor(private service: DashboardService) { }

  ngOnInit() {
    this.loadDashboard();
   }

   loadDashboard(){
      return this.service.getDashboardCount().subscribe((data: {}) => {
        this.Dashboards = data;
      });
   }

}



