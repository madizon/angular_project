import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { ApiPaths } from './../api-paths.enum';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  dashboardUrl = `${environment.baseURL}${ApiPaths.Dashboard}`;

  constructor(private httpClient: HttpClient) { }

  getDashboardCount() {
    return this.httpClient.get<JSON>(this.dashboardUrl);
  }
}
