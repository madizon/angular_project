import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  isLoggedIn: boolean;

  constructor(private auth: AuthService, private router: Router){ }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot){

    this.auth.userAuthState.subscribe(val => {
        this.isLoggedIn = val;
    });

    if (!this.isLoggedIn) {
        this.router.navigate(['login']);
        return false;
      }

    return true;
  }
}
