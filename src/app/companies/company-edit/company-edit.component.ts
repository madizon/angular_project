import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CompanyService } from './../company.service';

@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.css']
})
export class CompanyEditComponent implements OnInit {

  id = this.route.snapshot.params['id'];
  company: any = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: CompanyService
  ) {}


  ngOnInit() {
    this.loadCompany();
  }

  loadCompany(){
   return this.service.getCompany(this.id).subscribe((data) => {
       this.company = data;
    });
  }

  updateCompany() {
    if(window.confirm('Are you sure you want to update?')){
      this.service.updateCompany(this.company).subscribe(data => {
        this.router.navigate(['/companies']);
      });
    }
  }
}
