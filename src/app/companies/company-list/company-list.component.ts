import { Component, OnInit } from '@angular/core';
import { CompanyService } from './../company.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {
  companies: any = [];

  constructor(private service: CompanyService) { }

  ngOnInit() {
    this.loadCompanies();
   }

   loadCompanies(){
      return this.service.getCompanies().subscribe((data: {}) => {
        this.companies = data;
      });
   }

   trackByFn(index) {
    return index; 
    }

   deleteCompany(id) {
      if (window.confirm('Are you sure you want to delete?')){
         this.service.deleteCompany(id).subscribe(data => {
          this.loadCompanies();
        });
      }
    }  
}
