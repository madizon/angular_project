import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CompaniesRoutingModule } from './companies-routing.module';

import { CompanyDetailComponent } from './company-detail/company-detail.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyCreateComponent } from './company-create/company-create.component';
import { CompanyEditComponent } from './company-edit/company-edit.component';

@NgModule({
  declarations: [CompanyDetailComponent, CompanyListComponent, CompanyCreateComponent, CompanyEditComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    CompaniesRoutingModule
  ]
})
export class CompaniesModule { }
