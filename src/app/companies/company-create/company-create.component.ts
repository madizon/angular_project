import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyService } from './../company.service';

@Component({
  selector: 'app-company-create',
  templateUrl: './company-create.component.html',
  styleUrls: ['./company-create.component.css']
})
export class CompanyCreateComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private service: CompanyService) { }

  @Input() companyDetails = { name : null, email: null , website: null};

  ngOnInit(): void { }

  addCompany() {
    this.service.createCompany(this.companyDetails).subscribe((data: {}) => {
      this.router.navigate(['/companies']);
    });
  }
}
