import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompanyService } from './../company.service';

@Component({
  selector: 'app-company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.css']
})
export class CompanyDetailComponent implements OnInit {

  id = this.route.snapshot.params['id'];
  company: any = {};

  constructor(private route: ActivatedRoute, private service: CompanyService) {}

  ngOnInit(): void {
    this.loadCompany();
  }

  loadCompany(){
   return this.service.getCompany(this.id).subscribe((data) => {
       this.company = data;
    });
  }
}
