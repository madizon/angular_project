import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyDetailComponent } from './company-detail/company-detail.component';
import { CompanyEditComponent } from './company-edit/company-edit.component';
import { CompanyCreateComponent } from './company-create/company-create.component';

const companiesRoutes: Routes = [
  { path: 'companies',  component: CompanyListComponent },
  { path: 'companies/create', component: CompanyCreateComponent },
  { path: 'companies/:id', component: CompanyDetailComponent },
  { path: 'companies/:id/edit', component: CompanyEditComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(companiesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CompaniesRoutingModule { }
