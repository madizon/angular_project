import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable  } from 'rxjs';
import { Company } from './company';
import { environment } from './../../environments/environment';
import { ApiPaths } from './../api-paths.enum';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

    // Http Options
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

  companyUrl = `${environment.baseURL}${ApiPaths.Company}`;

  constructor(private httpClient: HttpClient) { }

  getCompanies(): Observable<Company>{
    return this.httpClient.get<Company>(this.companyUrl);
  }

  getCompany(id): Observable<Company> {
    return this.httpClient.get<Company>(this.companyUrl + '/' + id);
  }

  createCompany(company): Observable<Company> {
    return this.httpClient.post<Company>(this.companyUrl, JSON.stringify(company), this.httpOptions);
  }

  updateCompany(company): Observable<Company> {
    return this.httpClient.put<Company>(this.companyUrl + '/' + company.id, JSON.stringify(company), this.httpOptions);
  }

  deleteCompany(id){
    return this.httpClient.delete<Company>(this.companyUrl + '/' + id, this.httpOptions);
  }
}
